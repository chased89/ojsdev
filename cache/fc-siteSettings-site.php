<?php return array (
  'title' => 
  array (
    'en_US' => 'eJournal Publishing Services',
  ),
  'pageHeaderTitleType' => 
  array (
    'en_US' => '1',
  ),
  'siteTheme' => '',
  'showThumbnail' => true,
  'showTitle' => true,
  'showDescription' => true,
  'oneStepReset' => false,
  'useAlphalist' => false,
  'usePaging' => false,
  'defaultMetricType' => '',
  'preventManagerPluginManagement' => false,
  'intro' => 
  array (
    'en_US' => '<p style="margin-top: -1em;">Scholarly Communication’s epublishing services include hosting <a href="http://pkp.sfu.ca/">PKP</a>\'s Open Journal Systems and Open Conference Systems software and assisting with workflow design and overall production support.</p><ul><li><em>Improve access to research in your field by starting a new journal.</em></li></ul><ul><li><em>Expose a journal now hidden behind a pay wall.</em></li></ul><ul><li><em>Gather and publish your conference’s proceedings.</em></li></ul><p> Virginia Tech Libraries will help with these and more. We provide <a href="https://pkp.sfu.ca/ojs/">OJS</a> and <a href="https://econferencesdev.lib.vt.edu">OCS</a> to make journal production much less time-consuming, especially if you’ve been relying on manual labor to date. OJS helps with the call for papers, the peer review process, the publication of each issue, etc.</p><p>This management and publishing system can improve workflow but you still control and look and feel of your publication.</p><br /><a href="https://spectrajournal.org/index.php/SPECTRA" target="_blank"><img src="https://scholar.lib.vt.edu/images/SPECTRA15.png" alt="SPECTRA Logo" width="90%" /></a><p> </p><p><a href="https://spectrajournal.org/index.php/SPECTRA">SPECTRA</a> and <a href="/index.php/JRMP">Journal of Research in Music Performance</a>, edited by Kelly Parkes, Associate Professor of Music Education at Virginia Tech, are our first journals to use of OJS. Look for the <em>International Journal of Recirculating Aquaculture</em> coming soon.</p><p><strong><a href="http://scholar.lib.vt.edu/ejournals/">Complete List of Scholarly Communication Ejournals</a></strong></p><p>To take advantage of our services and resources, contact Gail McMillan: <a href="mailto:gailmac@vt.edu">gailmac@vt.edu</a>, Director of Scholarly Communication.</p>',
  ),
  'contactName' => 
  array (
    'en_US' => 'Open Journal Systems',
  ),
  'contactEmail' => 
  array (
    'en_US' => 'ojs@dla.lib.vt.edu',
  ),
  'pageHeaderTitleImage' => 
  array (
    'en_US' => 
    array (
      'originalFilename' => 'vt_header_screen_ojs.jpg',
      'width' => 758,
      'height' => 97,
      'uploadName' => 'pageHeaderTitleImage_en_US.jpg',
      'dateUploaded' => '2012-11-20 14:23:11',
      'altText' => '',
    ),
  ),
); ?>