<?php return array (
  'homeHeaderTitle' => 
  array (
    'en_US' => 'The ALAN Review',
  ),
  'pageHeaderTitle' => 
  array (
    'en_US' => 'The ALAN Review',
  ),
  'readerInformation' => 
  array (
    'en_US' => 'We encourage readers to sign up for the publishing notification service for this journal. Use the <a href="/ALAN/user/register">Register</a> link at the top of the home page for the journal. This registration will result in the reader receiving the Table of Contents by email for each new issue of the journal. This list also allows the journal to claim a certain level of support or readership. See the journal\'s <a href="/ALAN/about/submissions#privacyStatement">Privacy Statement</a>, which assures readers that their name and email address will not be used for other purposes.',
  ),
  'supportedSubmissionLocales' => 
  array (
    0 => 'en_US',
  ),
  'articleEventLog' => false,
  'articleEmailLog' => false,
  'authorInformation' => 
  array (
    'en_US' => 'Interested in submitting to this journal? We recommend that you review the <a href="/ALAN/about">About the Journal</a> page for the journal\'s section policies, as well as the <a href="/ALAN/about/submissions#authorGuidelines">Author Guidelines</a>. Authors need to <a href="/ALAN/user/register">register</a> with the journal prior to submitting or, if already registered, can simply <a href="/index/login">log in</a> and begin the five-step process.',
  ),
  'contactPhone' => '',
  'contactFax' => '',
  'supportName' => 'Chase Dooley',
  'supportEmail' => 'chasehd@vt.edu',
  'supportPhone' => '',
  'publisherInstitution' => 'Virginia Tech Libraries',
  'publisherUrl' => 'http://scholar.lib.vt.edu/',
  'includeCreativeCommons' => false,
  'contributors' => 
  array (
  ),
  'mailingAddress' => '',
  'useEditorialBoard' => false,
  'metaCoverage' => false,
  'metaType' => false,
  'copyrightNoticeAgree' => false,
  'requireAuthorCompetingInterests' => false,
  'contactEmail' => 'thealanreview@gmail.com',
  'requireReviewerCompetingInterests' => false,
  'metaDiscipline' => false,
  'metaSubjectClass' => false,
  'metaSubject' => false,
  'metaCitations' => true,
  'onlineIssn' => '1547-741',
  'openAccessPolicy' => 
  array (
    'en_US' => 'This journal provides immediate open access to its content on the principle that making research freely available to the public supports a greater global exchange of knowledge.',
  ),
  'librarianInformation' => 
  array (
    'en_US' => 'We encourage research librarians to list this journal among their library\'s electronic journal holdings. As well, it may be worth noting that this journal\'s open source publishing system is suitable for libraries to host for their faculty members to use with journals they are involved in editing (see <a href="http://pkp.sfu.ca/ojs">Open Journal Systems</a>).',
  ),
  'itemsPerPage' => 25,
  'numPageLinks' => 10,
  'enableAuthorSelfArchive' => false,
  'authorSelfArchivePolicy' => 
  array (
    'en_US' => 'This journal permits and encourages authors to post items submitted to the journal on personal websites or institutional repositories both prior to and after publication, while providing bibliographic details that credit, if applicable, its publication in this journal.',
  ),
  'subscriptionExpiryPartial' => true,
  'enableSubscriptionOnlinePaymentNotificationPurchaseIndividual' => false,
  'enableSubscriptionOnlinePaymentNotificationPurchaseInstitutional' => false,
  'enableSubscriptionOnlinePaymentNotificationRenewIndividual' => false,
  'enableSubscriptionOnlinePaymentNotificationRenewInstitutional' => false,
  'enableSubscriptionExpiryReminderBeforeMonths' => false,
  'numMonthsBeforeSubscriptionExpiryReminder' => 0,
  'journalTheme' => 'classicGreen',
  'enableSubscriptionExpiryReminderBeforeWeeks' => false,
  'numWeeksBeforeSubscriptionExpiryReminder' => 0,
  'enableSubscriptionExpiryReminderAfterMonths' => false,
  'numMonthsAfterSubscriptionExpiryReminder' => 0,
  'enableSubscriptionExpiryReminderAfterWeeks' => false,
  'numWeeksAfterSubscriptionExpiryReminder' => 0,
  'journalStyleSheet' => 
  array (
    'name' => 'sitestyle.css',
    'uploadName' => 'journalStyleSheet.css',
    'dateUploaded' => '2014-12-17 10:54:32',
  ),
  'remindForInvite' => true,
  'remindForSubmit' => true,
  'copySubmissionAckPrimaryContact' => false,
  'copySubmissionAckAddress' => 'alan-review@uconn.edu',
  'numDaysBeforeInviteReminder' => 3,
  'numDaysBeforeSubmitReminder' => 1,
  'envelopeSender' => 'alan-review@uconn.edu',
  'numWeeksPerReview' => 3,
  'copySubmissionAckSpecified' => true,
  'boardEnabled' => true,
  'metaCitationOutputFilterId' => -1,
  'publicationFormatYear' => true,
  'publicationFormatTitle' => true,
  'useLayoutEditors' => false,
  'provideRefLinkInstructions' => false,
  'description' => 
  array (
    'en_US' => '<p><em>The ALAN Review (TAR)</em> is a peer-reviewed (refereed) journal published by the Assembly on Literature for Adolescents of the National Council of Teachers of English (ALAN). It is devoted solely to the field of literature for young adults. It is published three times per academic year (fall, winter, and summer) and is sent to all ALAN members, individual and institutional. Members of ALAN need not be members of NCTE.</p><p><em>TAR</em> publishes high quality articles and professional materials that support the learning and development of readers committed to (or wanting to learn more about) young adult literature—its authors, its readers, and its advocates. <em>TAR</em> publishes scholarship and resource materials that build, expand, and challenge readers’ understandings, as well as support them in the daily work they do with the students in their care.</p>',
  ),
  'navItems' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'name' => '',
        'url' => '',
      ),
    ),
  ),
  'rateReviewerOnQuality' => true,
  'supportedLocales' => 
  array (
    0 => 'en_US',
  ),
  'supportedFormLocales' => 
  array (
    0 => 'en_US',
  ),
  'initialVolume' => 21,
  'initialNumber' => 3,
  'restrictSiteAccess' => false,
  'restrictArticleAccess' => false,
  'publicationFormatVolume' => true,
  'publicationFormatNumber' => true,
  'initialYear' => 1994,
  'useCopyeditors' => false,
  'printIssn' => '0882-2840',
  'contactName' => 'Wendy Glenn, Ricki Ginsberg, and Danielle King',
  'rtAbstract' => true,
  'rtCaptureCite' => true,
  'rtViewMetadata' => true,
  'rtSupplementaryFiles' => true,
  'rtPrinterFriendly' => true,
  'rtAuthorBio' => true,
  'rtDefineTerms' => true,
  'rtAddComment' => true,
  'rtEmailAuthor' => true,
  'rtEmailOthers' => true,
  'submissionFee' => 0,
  'submissionFeeName' => 
  array (
    'en_US' => 'Article Submission',
  ),
  'submissionFeeDescription' => 
  array (
    'en_US' => 'Authors are required to pay an Article Submission Fee as part of the submission process to contribute to review costs.',
  ),
  'fastTrackFee' => 0,
  'fastTrackFeeName' => 
  array (
    'en_US' => 'Fast-Track Review',
  ),
  'fastTrackFeeDescription' => 
  array (
    'en_US' => 'With the payment of this fee, the review, editorial decision, and author notification on this manuscript is guaranteed to take place within 4 weeks.',
  ),
  'publicationFee' => 0,
  'publicationFeeName' => 
  array (
    'en_US' => 'Article Publication',
  ),
  'publicationFeeDescription' => 
  array (
    'en_US' => 'If this paper is accepted for publication, you will be asked to pay an Article Publication Fee to cover publications costs.',
  ),
  'waiverPolicy' => 
  array (
    'en_US' => 'If you do not have funds to pay such fees, you will have an opportunity to waive each fee. We do not want fees to prevent the publication of worthy work.',
  ),
  'purchaseArticleFee' => 0,
  'purchaseArticleFeeName' => 
  array (
    'en_US' => 'Purchase Article',
  ),
  'purchaseArticleFeeDescription' => 
  array (
    'en_US' => 'The payment of this fee will enable you to view, download, and print this article.',
  ),
  'membershipFee' => 0,
  'membershipFeeName' => 
  array (
    'en_US' => 'Association Membership',
  ),
  'membershipFeeDescription' => 
  array (
    'en_US' => 'The payment of this fee will enroll you as a member in this association for one year and provide you with free access to this journal.',
  ),
  'donationFeeName' => 
  array (
    'en_US' => 'Donations to journal',
  ),
  'allowRegReader' => false,
  'allowRegAuthor' => true,
  'allowRegReviewer' => false,
  'restrictReviewerFileAccess' => true,
  'reviewerAccessKeysEnabled' => true,
  'useProofreaders' => false,
  'publishingMode' => 1,
  'showGalleyLinks' => true,
  'enableAnnouncements' => false,
  'enableAnnouncementsHomepage' => false,
  'numAnnouncementsHomepage' => 0,
  'volumePerYear' => 1,
  'issuePerVolume' => 3,
  'enablePublicIssueId' => false,
  'enablePublicArticleId' => false,
  'enablePublicGalleyId' => false,
  'enablePublicSuppFileId' => false,
  'enablePageNumber' => false,
  'focusScopeDesc' => 
  array (
    'en_US' => '<p><em>The ALAN Review (TAR)</em> is a peer-reviewed (refereed) journal published by the Assembly on Literature for Adolescents of the National Council of Teachers of English (ALAN). It is devoted solely to the field of literature for young adults. It is published three times per academic year (fall, winter, and summer) and is sent to all ALAN members, individual and institutional. Members of ALAN need not be members of NCTE.</p><p><em>TAR</em> publishes high quality articles and professional materials that support the learning and development of readers committed to (or wanting to learn more about) young adult literature—its authors, its readers, and its advocates. <em>TAR</em> publishes scholarship and resource materials that build, expand, and challenge readers’ understandings, as well as support them in the daily work they do with the students in their care.</p>',
  ),
  'reviewPolicy' => 
  array (
    'en_US' => '<div id="page-content" class="description"><div class="xg_user_generated">Manuscripts receive a blind review by the editors and at least three members of the Editorial Review Board, unless the length, style, or content makes it inappropriate for publication. Usually, authors should expect to hear the results within eight weeks. Manuscripts are judged for the contribution made to the field of young adult literature and mission of <em>The ALAN Review</em>, scholarly rigor, and clarity of writing. Selection also depends on the manuscript’s contribution to the overall balance of the journal.</div></div>',
  ),
  'reviewGuidelines' => 
  array (
    'en_US' => '<div id="content"><div id="post-117" class="post"><div class="entry"><div class="xg_headline"><div class="tb"><h3><strong style="font-size: 10px;"><em>The ALAN Review</em> Guidelines for Reviewers and Sample Review</strong></h3></div></div><div class="xg_module"><div class="xg_module_body wpage"><div id="page-content" class="description"><div class="xg_user_generated"><p style="text-align: left;" align="center">Thank you for agreeing to serve as a reviewer for <em>The ALAN Review</em>!  We appreciate your expertise and support of our authors. We employ a double-blind referee system: identities of the author(s) and the reviewers are not revealed to one another.  However, we include reviewers’ comments (or excerpts) in our responses to submitting author(s).</p><p>As you craft your response and make your recommendation, please consider the guiding questions below; these are aligned with the criteria included on the review form. You do not need to answer every question or organize your response according to this format.  We simply provide these to aid in your thinking.</p><p>1) Does the piece contribute to the field of young adult literature and mission of <em>TAR</em> by exploring, highlighting, and/or complicating theories and/or practices of young adult literature?</p><ul><li>Is the paper likely to ignite conversation within the profession?</li><li>Is the paper of potential interest to education professionals across a range of institutional contexts and levels?</li><li>Are the implications of the paper potentially significant?</li></ul><p>2) Does the piece demonstrate scholarly rigor?</p><ul><li>Is the author’s understanding of the subject informed by current scholarship?</li><li>Does the author employ methods that are appropriate to the question(s) being asked? Are these methods clearly, thoroughly, and appropriately articulated?</li><li>Does the author provide a clear and well-supported argument for claims made?</li></ul><p>3) Does the piece demonstrate clarity of writing?</p><ul><li>Is the piece written in a tone likely to invite readers’ interest?</li><li>Is the piece organized in such a way that helps readers navigate the argument made?</li><li>Is the piece free from errors in language, punctuation, grammar, usage, etc. that impede readers’ understandings?</li></ul><p>In your response to the author(s), we encourage you to explain your thinking and comment upon both strengths of the paper and suggestions for revision.  We aim for the review process to be educative and positive for all authors. Below, we include a sample response for your use. Again, your review does not need to follow this format.  We share this as one model to consider.  As always, please don’t hesitate to send any questions our way.</p><p><strong>Sample Review</strong></p><p style="padding-left: 60px;">Dear Author(s),</p><p style="padding-left: 60px;">Thank you for sharing your thoughtful work on an important topic.  It is clear that you and your participants gained much through the book club experience.  I found particularly relevant the ways in which your study attests to the power of engagement with literature to extend beyond development of readers’ skills and into socio-cultural understandings of the self and others.  This intimates the need for and value of those who engage in literature study with students to actively foster critical reflection that makes transparent the underlying messages of power, privilege, and gendered identities that texts inherently contain.  I appreciate, too, the clarity of your writing.  The subheadings in the Findings section, in particular, help readers follow the progression of your argument from one section to the next.</p><p style="padding-left: 60px;">To make your argument clearer and more compelling to readers, I encourage you to better align the various elements of your piece.  In the Discussion, you state that you conducted a “***” (p.), that your findings “***” (p.), and that your work underpins the value of “***” (p.).  While these are indeed important contributions to the field, I’m not entirely convinced that you’ve created the necessary argument to be able to make these claims in the way you’ve currently presented your study.  I recommend that you consider the following:</p><p style="padding-left: 60px;">1) The Literature section provides important contextual information that grounds your study relative to girls’ literacy, gendered texts, and book clubs.  However, given the emphasis on power and privilege in the Discussion (an important emphasis), I encourage you to describe research that addresses these issues in the field as they relate to your work to more accurately frame your study’s goals.</p><p style="padding-left: 60px;">2) You provide a strong general statement of summary regarding your work (p.), but how you came to the answer, “***,” is unclear.  I recommend that you explicitly state your research question and then, in the Findings, demonstrate clearly how your data reflect the development of your answer to this question.</p><p style="padding-left: 60px;">3) I appreciate the level of detail included in your Findings.  Hearing the participants’ voices throughout this section makes your work compelling and engaging; you help us care about them and their resulting perceptions that grow out of examination of the text.  For each finding, however, I’d love to see you focus more intentionally on gender and relate your findings back to your Literature Review.  You touch on this in your discussion of *** when you note, “***” (p.).  How might you develop your analysis of this claim to more clearly address what it means to be a female in this community?  Similarly, in the discussion of ***, you might explore the ways in which the term seems to be gender-specific, often present in female discourse but rarely, if ever, in male discourse.  And in the section on ***, I was fascinated by the ways in which the girls rejected a potentially physical response because it would not be approved and instead suggested a more socially acceptable, passive response.  Again, how might this speak to the roles of girls and boys in the school environment in which they exist?</p><p style="padding-left: 60px;">4) As indicated earlier, your Discussion contains evidence of careful analysis and thinking that moves the conversation forward.  At times, however, this section reads more like literary analysis, particularly when you discuss the representation of girls the selected text (and other literature) contains (p.).  This information might be important but would feel essential to your work if it were more carefully connected to your Findings and an expanded Lit Review that addresses power and privilege.</p><p style="padding-left: 60px;">Beyond the issue of alignment, I encourage you to delineate more clearly (and with examples) the processes of analysis that led to these particular findings.  Perhaps provide examples of your thinking during the analysis process (Why was a particular piece of data categorized in one way and not another? Did your categories change as the analysis progressed? Were any data eliminated given a lack of richness across sources? etc.).</p><p style="padding-left: 60px;">Thank you again for your willingness to submit your study for review.  I appreciate the opportunity to read your important work and wish you all the best.</p></div></div></div></div></div></div></div>',
  ),
  'privacyStatement' => 
  array (
    'en_US' => 'The names and email addresses entered in this journal site will be used exclusively for the stated purposes of this journal and will not be made available for any other purpose or to any other party.',
  ),
  'customAboutItems' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'title' => 'Editorial Board',
        'content' => '<p><strong><span style="font-size: 1em;">Editorial Review Board</span></strong></p><p><span style="font-size: 1em; line-height: 2em;">Mary Arnold, </span><em style="font-size: 1em; line-height: 2em;">Cuyahoga County Public Library</em><br /><span style="line-height: 2em;">Jacqueline Bach, </span><em style="line-height: 2em;">Louisiana State University</em><br /><span style="line-height: 2em;">Katherine Bell, </span><em style="line-height: 2em;">Wilfrid Laurier University</em><br /><span style="line-height: 2em;">Steven Bickmore, </span><em style="line-height: 2em;">Louisiana State University</em><br /><span style="line-height: 2em;">Joni Richards Bodart, </span><em style="line-height: 2em;">San Jose State University</em><br /><span style="line-height: 2em;">Jean Boreen, </span><em style="line-height: 2em;">Northern Arizona University</em><br /><span style="line-height: 2em;">April Brannon, </span><em style="line-height: 2em;">California State University, Fullerton</em><br /><span style="line-height: 2em;">Kathy Brashears, </span><em style="line-height: 2em;">Tennessee Tech University</em><br /><span style="line-height: 2em;">Alan Brown, </span><em style="line-height: 2em;">Wake Forest University</em><br /><span style="line-height: 2em;">Bill Broz, </span><em style="line-height: 2em;">University of Texas-Pan American</em><br /><span style="line-height: 2em;">Jennifer Buehler, </span><em style="line-height: 2em;">Saint Louis University</em><br /><span style="line-height: 2em;">Tara Campbell, </span><em style="line-height: 2em;">Georgia State University</em><br /><span style="line-height: 2em;">James Bucky Carter, </span><em style="line-height: 2em;">Washington State University</em><br /><span style="line-height: 2em;">James S. Chisholm, </span><em style="line-height: 2em;">University of Louisville</em><br /><span style="line-height: 2em;">Melissa Comer, </span><em style="line-height: 2em;">Tennessee Tech University</em><br /><span style="line-height: 2em;">Sean Connors, </span><em style="line-height: 2em;">University of Arkansas</em><br /><span style="line-height: 2em;">Chris Crowe, </span><em style="line-height: 2em;">Brigham Young University</em><br /><span style="line-height: 2em;">Jennifer Dail, </span><em style="line-height: 2em;">Kennesaw State University</em><br /><span style="line-height: 2em;">Janine Darragh, </span><em style="line-height: 2em;">University of Idaho</em><br /><span style="line-height: 2em;">Jennifer Denmon, </span><em style="line-height: 2em;">University of South Florida</em><br /><span style="line-height: 2em;">Maggie Dorsey, </span><em style="line-height: 2em;">Oxford Academy</em><br /><span style="line-height: 2em;">Katie S. Dredger, </span><em style="line-height: 2em;">James Madison University</em><br /><span style="line-height: 2em;">Kevin Dupre, </span><em style="line-height: 2em;">Athens State University</em><br /><span style="line-height: 2em;">E. Sybil Durand, </span><em style="line-height: 2em;">Arizona State University</em><br /><span style="line-height: 2em;">Brooke Eisenbach, </span><em style="line-height: 2em;">Florida Virtual School and University of South Florida</em><br /><span style="line-height: 2em;">Toby Emert, </span><em style="line-height: 2em;">Agnes Scott College</em><br /><span style="line-height: 2em;">Judith Franzak, </span><em style="line-height: 2em;">New Mexico State University</em><br /><span style="line-height: 2em;">Maggie Freeburn, </span><em style="line-height: 2em;">Formerly University of Colorado Denver</em><br /><span style="line-height: 2em;">Bryan Gillis, </span><em style="line-height: 2em;">Kennesaw State University</em><br /><span style="line-height: 2em;">Mary Louise Gomez, </span><em style="line-height: 2em;">University of Wisconsin-Madison</em><br /><span style="line-height: 2em;">Robin D. Groce, </span><em style="line-height: 2em;">Appalachian State University</em><br /><span style="line-height: 2em;">Kay Haas, </span><em style="line-height: 2em;">Johnson County Community College</em><br /><span style="line-height: 2em;">Patricia M. Hauschildt, </span><em style="line-height: 2em;">Youngstown State University</em><br /><span style="line-height: 2em;">Judith Hayn, </span><em style="line-height: 2em;">University of Arkansas at Little Rock</em><br /><span style="line-height: 2em;">Lisa A. Hazlett, </span><em style="line-height: 2em;">University of South Dakota</em><br /><span style="line-height: 2em;">Kathy Headley, </span><em style="line-height: 2em;">Clemson University</em><br /><span style="line-height: 2em;">Sarah K. Herz, </span><em style="line-height: 2em;">Westport Public Schools</em><br /><span style="line-height: 2em;">Crag Hill, </span><em style="line-height: 2em;">University of Oklahoma</em><br /><span style="line-height: 2em;">KaaVonia Hinton, </span><em style="line-height: 2em;">Old Dominion University</em><br /><span style="line-height: 2em;">Teri Holbrook, </span><em style="line-height: 2em;">Georgia State University</em><br /><span style="line-height: 2em;">Peggy F. (Missy) Hopper, </span><em style="line-height: 2em;">Mississippi State University</em><br /><span style="line-height: 2em;">Melanie Hundley, </span><em style="line-height: 2em;">Vanderbilt University</em><br /><span style="line-height: 2em;">Angela Insenga, </span><em style="line-height: 2em;">University of West Georgia</em><br /><span style="line-height: 2em;">Angie Beumer Johnson, </span><em style="line-height: 2em;">Wright State University</em><br /><span style="line-height: 2em;">Jeffrey Kaplan, </span><em style="line-height: 2em;">University of Central Florida, Orlando</em><br /><span style="line-height: 2em;">Joan F. Kaywell, </span><em style="line-height: 2em;">University of South Florida</em><br /><span style="line-height: 2em;">Brian Kelley, </span><em style="line-height: 2em;">Fordham University</em><br /><span style="line-height: 2em;">Kathryn Kelly, </span><em style="line-height: 2em;">Radford University</em><br /><span style="line-height: 2em;">Dixie Keyes, </span><em style="line-height: 2em;">Arkansas State University</em><br /><span style="line-height: 2em;">Melanie Koss, </span><em style="line-height: 2em;">Northern Illinois University</em><br /><span style="line-height: 2em;">Sean Kottke, </span><em style="line-height: 2em;">Michigan Department of Education</em><br /><span style="line-height: 2em;">Desi Krell, </span><em style="line-height: 2em;">University of Florida</em><br /><span style="line-height: 2em;">Courtney M. Krieger, </span><em style="line-height: 2em;">Western Heights Middle School and Hillsdale College</em><br /><span style="line-height: 2em;">Nai-Hua Kuo, </span><em style="line-height: 2em;">Purdue University</em><br /><span style="line-height: 2em;">Teri Lesesne, </span><em style="line-height: 2em;">Sam Houston State University</em><br /><span style="line-height: 2em;">Mark Letcher, </span><em style="line-height: 2em;">Purdue University, Calumet</em><br /><span style="line-height: 2em;">Marilyn Lorch, </span><em style="line-height: 2em;">Northern Illinois University</em><br /><span style="line-height: 2em;">Joellen Maples,</span><em style="line-height: 2em;"> St. John Fisher College</em><br /><span style="line-height: 2em;">Katherine Mason, </span><em style="line-height: 2em;">Wichita State University</em><br /><span style="line-height: 2em;">Sharron L. McElmeel, </span><em style="line-height: 2em;">University of Wisconsin-Stout</em><br /><span style="line-height: 2em;">SJ Miller, </span><em style="line-height: 2em;">University of Colorado, Boulder</em><br /><span style="line-height: 2em;">J. Bradley Minnick, </span><em style="line-height: 2em;">University of Arkansas at Little Rock</em><br /><span style="line-height: 2em;">John Noell Moore, </span><em style="line-height: 2em;">The College of William and Mary</em><br /><span style="line-height: 2em;">Shannon R. Mortimore-Smith, </span><em style="line-height: 2em;">Shippensburg University</em><br /><span style="line-height: 2em;">Kellee Moye, </span><em style="line-height: 2em;">Hunter’s Creek Middle School</em><br /><span style="line-height: 2em;">Alleen Nilsen, </span><em style="line-height: 2em;">Arizona State University</em><br /><span style="line-height: 2em;">Elaine J. O’Quinn, </span><em style="line-height: 2em;">Appalachian State University</em><br /><span style="line-height: 2em;">Mary Ellen Oslick, </span><em style="line-height: 2em;">University of Central Arkansas</em><br /><span style="line-height: 2em;">Jon Ostenson, </span><em style="line-height: 2em;">Brigham Young University</em><br /><span style="line-height: 2em;">Ruchelle Owens, </span><em style="line-height: 2em;">University of South Florida and Tomlin Middle School</em><br /><span style="line-height: 2em;">Linda T. Parsons, </span><em style="line-height: 2em;">The Ohio State University, Marion</em><br /><span style="line-height: 2em;">Sue Christian Parsons, </span><em style="line-height: 2em;">Oklahoma State University</em><br /><span style="line-height: 2em;">Emily Pendergrass, </span><em style="line-height: 2em;">Vanderbilt University</em><br /><span style="line-height: 2em;">Karin Perry, </span><em style="line-height: 2em;">Sam Houston State University</em><br /><span style="line-height: 2em;">Daria Plumb, </span><em style="line-height: 2em;">Riverside Academy</em><br /><span style="line-height: 2em;">June Pulliam, </span><em style="line-height: 2em;">Louisiana State University</em><br /><span style="line-height: 2em;">Delia Regan, </span><em style="line-height: 2em;">East Hampton Middle School</em><br /><span style="line-height: 2em;">Kia Jane Richmond, </span><em style="line-height: 2em;">Northern Michigan University</em><br /><span style="line-height: 2em;">René Saldaña, Jr., </span><em style="line-height: 2em;">Texas Tech University</em><br /><span style="line-height: 2em;">Sophia Sarigianides, </span><em style="line-height: 2em;">Westfield State University</em><br /><span style="line-height: 2em;">Lisa Scherff, </span><em style="line-height: 2em;">Estero High School</em><br /><span style="line-height: 2em;">Renita Schmidt, </span><em style="line-height: 2em;">The University of Iowa</em><br /><span style="line-height: 2em;">Kelly Shea, </span><em style="line-height: 2em;">Manchester High School</em><br /><span style="line-height: 2em;">Joanna Simpson, </span><em style="line-height: 2em;">Kennesaw State University</em><br /><span style="line-height: 2em;">Tiffany Smith, </span><em style="line-height: 2em;">E. O. Smith High School</em><br /><span style="line-height: 2em;">Anna O. Soter, </span><em style="line-height: 2em;">The Ohio State University</em><br /><span style="line-height: 2em;">Kat Spradlin, </span><em style="line-height: 2em;">Godby High School</em><br /><span style="line-height: 2em;">Julie Stepp, </span><em style="line-height: 2em;">Tennessee Tech University</em><br /><span style="line-height: 2em;">Renee Stites, </span><em style="line-height: 2em;">Saint Louis University</em><br /><span style="line-height: 2em;">Lois T. Stover, </span><em style="line-height: 2em;">Marymount University</em><br /><span style="line-height: 2em;">Terri Suico,</span><em style="line-height: 2em;"> Saint Mary’s College</em><br /><span style="line-height: 2em;">Alan B. Teasley, </span><em style="line-height: 2em;">Duke University</em><br /><span style="line-height: 2em;">Mary Ann Tighe, </span><em style="line-height: 2em;">Troy University</em><br /><span style="line-height: 2em;">Eli Tucker-Raymond, </span><em style="line-height: 2em;">TERC</em><br /><span style="line-height: 2em;">Anete Vasquez, </span><em style="line-height: 2em;">Kennesaw State University</em><br /><span style="line-height: 2em;">Barbara A. Ward, </span><em style="line-height: 2em;">Washington State University</em><br /><span style="line-height: 2em;">Mary Warner, </span><em style="line-height: 2em;">San Jose State University</em><br /><span style="line-height: 2em;">Julie Warner, </span><em style="line-height: 2em;">Armstrong Atlantic State University</em><br /><span style="line-height: 2em;">Elizabeth Watts, </span><em style="line-height: 2em;">Broward County Public Schools</em><br /><span style="line-height: 2em;">Susan Weinstein, </span><em style="line-height: 2em;">Louisiana State University</em><br /><span style="line-height: 2em;">Marney Welmers, </span><em style="line-height: 2em;">Tortolita Middle School</em><br /><span style="line-height: 2em;">Nance Wilson, </span><em style="line-height: 2em;">SUNY Cortland</em><br /><span style="line-height: 2em;">Shelbie Witte, </span><em style="line-height: 2em;">Florida State University<br /></em><span style="line-height: 2em;">Connie S. Zitlow, </span><em style="line-height: 2em;"><em style="line-height: 2em;">Ohio Wesleyan University<br /></em></em><span style="line-height: 2em;">Jillian Zabrocky, </span><em style="line-height: 2em;"><em style="line-height: 2em;"><em style="line-height: 2em;">Ridgefield Public Schools</em></em></em></p><em style="line-height: 2em;"></em><p style="line-height: 2em;"> </p>',
      ),
      1 => 
      array (
        'title' => '',
        'content' => '',
      ),
    ),
  ),
  'lockssLicense' => 
  array (
    'en_US' => 'This journal utilizes the LOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href="http://lockss.org/">More...</a>',
  ),
  'sponsors' => 
  array (
    0 => 
    array (
      'institution' => 'Assembly on Literature for Adolescents, National Council of Teachers of English',
      'url' => 'http://www.alan-ya.org/the-alan-review/',
    ),
  ),
  'displayCurrentIssue' => true,
  'donationFeeDescription' => 
  array (
    'en_US' => 'Donations of any amount to this journal are gratefully received and provide a means for the editors to continue to provide a journal of the highest quality to its readers.',
  ),
  'subscriptionName' => '',
  'subscriptionEmail' => '',
  'subscriptionPhone' => '',
  'subscriptionFax' => '',
  'subscriptionMailingAddress' => '',
  'enableDelayedOpenAccess' => true,
  'delayedOpenAccessDuration' => 24,
  'enableOpenAccessNotification' => false,
  'showEnsuringLink' => true,
  'mailSubmissionsToReviewers' => false,
  'authorSelectsEditor' => false,
  'enableLockss' => false,
  'reviewerDatabaseLinks' => 
  array (
    0 => 
    array (
      'title' => '',
      'url' => '',
    ),
  ),
  'notifyAllAuthorsOnDecision' => true,
  'authorGuidelines' => 
  array (
    'en_US' => '<p><strong>ABOUT <em>THE ALAN REVIEW</em></strong></p><p><em>The ALAN Review (TAR)</em> publishes articles that explore, examine, critique, and advocate for literature for young adults and the teaching of that literature.  Published pieces include, but are not limited to, research studies, papers presented at professional meetings, surveys of the literature, critiques of the literature, articles about authors, comparative studies across genres and/or cultures, articles on ways to teach the literature to adolescents, and interviews with YA authors.</p><p><strong>AUDIENCE</strong></p><p>Many of the individual members of ALAN are classroom teachers of English in middle, junior, and senior high schools. Other readers include university faculty members in English and/or Education programs, researchers in the field of young adult literature, librarians, YA authors, publishers, reading teachers, and teachers in related content areas. ALAN has members in all 50 United States and a number of foreign countries.</p><p><strong>PREFERRED STYLE</strong></p><p>Manuscripts should usually be no longer than twenty double-spaced, typed pages, not including references. A manuscript submitted for consideration should deal specifically with literature for young adults and/or the teaching of that literature. It should have a clearly defined topic and be scholarly in content, as well as practical and useful to people working with and/or studying young adults and their literature. Research studies and papers should be treated as articles rather than formal reports. Stereotyping on the basis of sex, race, age, etc., should be avoided, as should gender-specific terms such as “chairman.”</p><p><strong>MANUSCRIPT FORMAT</strong></p><p>Manuscripts should be double-spaced throughout, including quotations and bibliographies.  The names of submitting authors should not appear anywhere in the manuscript. Short quotations, as permitted under “fair use” in the copyright law, must be carefully documented within the manuscript and in the bibliography. Longer quotations and complete poems or short stories must be accompanied by written permission from the copyright owner. YA author interviews should be accompanied by written permission for publication in <em>TAR</em> from the interviewed author(s). Interviewers should indicate to the author(s) that publication is subject to review of an editorial board. Original short tables and figures should be double-spaced and placed on separate sheets at the end of the manuscript. Notations should appear in the text indicating proper placement of tables and figures.</p><p><em>The ALAN Review</em> uses the bibliography style detailed in the <em>Publications Manual of the American Psychological Association</em> (APA). Please adhere to that style when including in-text citations and preparing reference lists.</p><p><strong>SUBMITTING THE MANUSCRIPT</strong></p><p>Authors should submit manuscripts electronically through this online system.  All manuscripts should be written using a recent version of Microsoft Word and use APA bibliographical format. Complete submissions include the following documents: 1) An abstract of no more than 150 words; 2) A manuscript without references to the author(s) and with name(s) removed in the Properties section under the File menu to ensure the piece is blinded; 3) A title page with names, affiliations, mailing addresses, and 100-150 word professional biographies for each submitting author, as well as a brief statement that the article is original, has not been published previously in other journals and/or books, and is not a simultaneous submission.</p><p><strong>REVIEW PROCESS</strong></p><p>Each manuscript will receive a blind review by the editors and at least three members of the Editorial Review Board, unless the length, style, or content makes it inappropriate for publication. Usually, authors should expect to hear the results within eight weeks. Manuscripts are judged for the contribution made to the field of young adult literature and mission of <em>The ALAN Review</em>, scholarly rigor, and clarity of writing. Selection also depends on the manuscript’s contribution to the overall balance of the journal.</p><p><strong>PUBLICATION OF ARTICLES</strong></p><p><em>The ALAN Review</em> assumes that accepted manuscripts have not been published previously in any other journals and/or books, nor will they be published subsequently without permission of the editors. Should the author submit the manuscript to more than one publication, he/she should notify <em>The ALAN Review</em>. If a submitted or accepted manuscript is accepted by another publication prior to publication in <em>The ALAN Review</em>, the author should immediately withdraw the manuscript from publication in <em>The ALAN Review</em>.</p><p>Manuscripts that are accepted may be edited for clarity, accuracy, readability, and publication style. Upon publication, the author will receive two copies of <em>The ALAN Review</em> in which the article appears. Publication usually occurs within 18 months of acceptance.<strong></strong></p><p><strong>DEADLINES.</strong></p><p><strong> </strong>Please observe these deadlines if you wish to have your article considered for a particular issue of <em>The ALAN Review</em>.</p><p><strong>FALL (October) ISSUE Deadline: MARCH 1</strong><br /><strong> WINTER (March) ISSUE Deadline: JULY 1</strong><br /><strong> SUMMER (June) ISSUE Deadline: NOVEMBER 1</strong></p>',
  ),
  'submissionChecklist' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'order' => '1',
        'content' => 'An abstract of no more than 150 words.',
      ),
      1 => 
      array (
        'order' => '2',
        'content' => 'A manuscript without references to the author(s).',
      ),
      2 => 
      array (
        'order' => '3',
        'content' => 'A title page with names, affiliations, mailing addresses, and 100-150 word professional biographies for each submitting author, as well as a brief statement that the article is original, has not been published previously in other journals and/or books, and is not a simultaneous submission (submitted as a Supplemental File).',
      ),
    ),
  ),
  'includeCopyrightStatement' => false,
  'licenseURL' => '',
  'includeLicense' => false,
  'copyrightHolderType' => '',
  'title' => 
  array (
    'en_US' => 'The ALAN Review',
  ),
  'initials' => 
  array (
    'en_US' => 'TAR',
  ),
  'abbreviation' => 
  array (
    'en_US' => 'ALAN',
  ),
  'categories' => NULL,
  'contactTitle' => 
  array (
    'en_US' => 'Editorial Team',
  ),
  'contactAffiliation' => 
  array (
    'en_US' => 'University of Connecticut',
  ),
  'history' => 
  array (
    'en_US' => '<em></em><p><em>The ALAN Review (TAR)</em> is a peer-reviewed (refereed) journal published by the Assembly on Literature for Adolescents of the National Council of Teachers of English (ALAN). It is devoted solely to the field of literature for young adults. It is published three times per academic year (fall, winter, and summer) and is sent to all ALAN members, individual and institutional. Members of ALAN need not be members of NCTE.</p><p><em>TAR</em> publishes high quality articles and professional materials that support the learning and development of readers committed to (or wanting to learn more about) young adult literature—its authors, its readers, and its advocates. <em>TAR</em> publishes scholarship and resource materials that build, expand, and challenge readers’ understandings, as well as support them in the daily work they do with the students in their care.</p>',
  ),
  'emailSignature' => '________________________________________________________________________
The ALAN Review
http://ejournals.lib.vt.edu/ALAN',
  'searchKeywords' => 
  array (
    'en_US' => 'young adult literature; YA literature, adolescent literature',
  ),
  'disableUserReg' => false,
  'journalThumbnail' => 
  array (
    'en_US' => 
    array (
      'name' => 'alan cover24.jpg',
      'uploadName' => 'journalThumbnail_en_US.jpg',
      'width' => 212,
      'height' => 275,
      'mimeType' => 'image/jpeg',
      'dateUploaded' => '2015-01-06 11:53:42',
    ),
  ),
  'copyeditInstructions' => 
  array (
    'en_US' => 'The copyediting stage is intended to improve the flow, clarity, grammar, wording, and formatting of the article. It represents the last chance for the author to make any substantial changes to the text because the next stage is restricted to typos and formatting corrections. The file to be copyedited is in Word or .rtf format and therefore can easily be edited as a word processing document. The set of instructions displayed here proposes two approaches to copyediting. One is based on Microsoft Word\'s Track Changes feature and requires that the copy editor, editor, and author have access to this program. A second system, which is software independent, has been borrowed, with permission, from the Harvard Educational Review. The journal editor is in a position to modify these instructions, so suggestions can be made to improve the process for this journal.<h4>Copyediting Systems</h4><strong>1. Microsoft Word\'s Track Changes</strong> Under Tools in the menu bar, the feature Track Changes enables the copy editor to make insertions (text appears in color) and deletions (text appears crossed out in color or in the margins as deleted). The copy editor can posit queries to both the author (Author Queries) and to the editor (Editor Queries) by inserting these queries in square brackets. The copyedited version is then uploaded, and the editor is notified. The editor then reviews the text and notifies the author. The editor and author should leave those changes with which they are satisfied. If further changes are necessary, the editor and author can make changes to the initial insertions or deletions, as well as make new insertions or deletions elsewhere in the text. Authors and editors should respond to each of the queries addressed to them, with responses placed inside the square brackets. After the text has been reviewed by editor and author, the copy editor will make a final pass over the text accepting the changes in preparation for the layout and galley stage. <strong>2. Harvard Educational Review </strong> <strong>Instructions for Making Electronic Revisions to the Manuscript</strong> Please follow the following protocol for making electronic revisions to your manuscript: <strong>Responding to suggested changes.</strong> For each of the suggested changes that you accept, unbold the text.   For each of the suggested changes that you do not accept, re-enter the original text and <strong>bold</strong> it. <strong>Making additions and deletions.</strong> Indicate additions by <strong>bolding</strong> the new text.   Replace deleted sections with: <strong>[deleted text]</strong>.   If you delete one or more sentence, please indicate with a note, e.g., <strong>[deleted 2 sentences]</strong>. <strong>Responding to Queries to the Author (QAs).</strong> Keep all QAs intact and bolded within the text. Do not delete them.   To reply to a QA, add a comment after it. Comments should be delimited using: <strong>[Comment:]</strong> e.g., <strong>[Comment: Expanded discussion of methodology as you suggested]</strong>. <strong>Making comments.</strong> Use comments to explain organizational changes or major revisions   e.g., <strong>[Comment: Moved the above paragraph from p. 5 to p. 7].</strong>Note: When referring to page numbers, please use the page numbers from the printed copy of the manuscript that was sent to you. This is important since page numbers may change as a document is revised electronically.<h4>An Illustration of an Electronic Revision</h4><ol><li><strong>Initial copyedit.</strong> The journal copy editor will edit the text to improve flow, clarity, grammar, wording, and formatting, as well as including author queries as necessary. Once the initial edit is complete, the copy editor will upload the revised document through the journal Web site and notify the author that the edited manuscript is available for review.</li><li><strong>Author copyedit.</strong> Before making dramatic departures from the structure and organization of the edited manuscript, authors must check in with the editors who are co-chairing the piece. Authors should accept/reject any changes made during the initial copyediting, as appropriate, and respond to all author queries. When finished with the revisions, authors should rename the file from AuthorNameQA.doc to AuthorNameQAR.doc (e.g., from LeeQA.doc to LeeQAR.doc) and upload the revised document through the journal Web site as directed.</li><li><strong>Final copyedit.</strong> The journal copy editor will verify changes made by the author and incorporate the responses to the author queries to create a final manuscript. When finished, the copy editor will upload the final document through the journal Web site and alert the layout editor to complete formatting.</li></ol>',
  ),
  'refLinkInstructions' => 
  array (
    'en_US' => '<h4>To Add Reference Linking to the Layout Process</h4><p>When turning a submission into HTML or PDF, make sure that all hyperlinks in the submission are active.</p><h4>A. When the Author Provides a Link with the Reference</h4><ol><li>While the submission is still in its word processing format (e.g., Word), add the phrase VIEW ITEM to the end of the reference that has a URL.</li><li>Turn that phrase into a hyperlink by highlighting it and using Word\'s Insert Hyperlink tool and the URL prepared in #2.</li></ol><h4>B. Enabling Readers to Search Google Scholar For References</h4><ol><li>While the submission is still in its word processing format (e.g., Word), copy the title of the work referenced in the References list (if it appears to be too common a title—e.g., "Peace"—then copy author and title).</li><li>Paste the reference\'s title between the %22\'s, placing a + between each word: http://scholar.google.com/scholar?q=%22PASTE+TITLE+HERE%22&amp;hl=en&amp;lr=&amp;btnG=Search.</li><li>Add the phrase GS SEARCH to the end of each citation in the submission\'s References list.</li><li>Turn that phrase into a hyperlink by highlighting it and using Word\'s Insert Hyperlink tool and the URL prepared in #2.</li></ol><h4>C. Enabling Readers to Search for References with a DOI</h4><ol><li>While the submission is still in Word, copy a batch of references into CrossRef Text Query http://www.crossref.org/freeTextQuery/.</li><li>Paste each DOI that the Query provides in the following URL (between = and &amp;): http://www.cmaj.ca/cgi/external_ref?access_num=PASTE DOI#HERE&amp;link_type=DOI.</li><li>Add the phrase CrossRef to the end of each citation in the submission\'s References list.</li><li>Turn that phrase into a hyperlink by highlighting the phrase and using Word\'s Insert Hyperlink tool and the appropriate URL prepared in #2.</li></ol>',
  ),
  'proofInstructions' => 
  array (
    'en_US' => '<p>The proofreading stage is intended to catch any errors in the galley\'s spelling, grammar, and formatting. More substantial changes cannot be made at this stage, unless discussed with the Section Editor. In Layout, click on VIEW PROOF to see the HTML, PDF, and other available file formats used in publishing this item.</p><h4>For Spelling and Grammar Errors</h4><p>Copy the problem word or groups of words and paste them into the Proofreading Corrections box with "CHANGE-TO" instructions to the editor as follows:</p><pre>1. CHANGE...
	then the others
	TO...
	than the others</pre><br /><pre>2. CHANGE...
	Malinowsky
	TO...
	Malinowski</pre><br /><h4>For Formatting Errors</h4><p>Describe the location and nature of the problem in the Proofreading Corrections box after typing in the title "FORMATTING" as follows:</p><br /><pre>3. FORMATTING
	The numbers in Table 3 are not aligned in the third column.</pre><br /><pre>4. FORMATTING
	The paragraph that begins "This last topic..." is not indented.</pre>',
  ),
  'homeHeaderLogoImage' => 
  array (
    'en_US' => 
    array (
      'name' => 'alan cover24.jpg',
      'uploadName' => 'homeHeaderLogoImage_en_US.jpg',
      'width' => 212,
      'height' => 275,
      'mimeType' => 'image/jpeg',
      'dateUploaded' => '2015-01-06 11:53:26',
    ),
  ),
  'pageHeaderLogoImage' => 
  array (
    'en_US' => 
    array (
      'name' => 'alan cover24.jpg',
      'uploadName' => 'pageHeaderLogoImage_en_US.jpg',
      'width' => 212,
      'height' => 275,
      'mimeType' => 'image/jpeg',
      'dateUploaded' => '2015-01-06 11:53:57',
    ),
  ),
); ?>